#include <RHRouter.h>
#include <RH_NRF905.h>
#include <SPI.h>
#include <ArduinoJson.h>

#define RH_ROUTER_MAX_MESSAGE_LEN 22

#define mobile_node_address 2
#define static_node_address 1
#define inter_node_address 3

// Singleton instance of the radio driver
RH_NRF905 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHRouter manager(driver, mobile_node_address);

struct sensor_data {
  float h;
  float t;
};

struct gps_data {
  double latitude;
  double longitude;
};

struct sensor_data sensor;
struct gps_data gps;

uint8_t send_len = sizeof(gps);

// Dont put this on the stack:
uint8_t buf[RH_ROUTER_MAX_MESSAGE_LEN];

uint recv_packets = 0;
uint trans_packets = 0;
uint retrans_packets = 0;
uint total_trans_packets = 0;
float trans_rate = 0.0;
String json_output;
String json_input;
// uint error_code = 5;

void readGpsData() {
  String json_input;
  // json_input = "{\"lat\": 52.5098988, \"lon\": 13.37319888}";
  // while (!Serial.available()){};
  if (Serial.available()) {
    json_input = Serial.readStringUntil('\n');
    // Serial.print(json_input+"\n");
    StaticJsonDocument<96> doc;
    deserializeJson(doc, json_input);
    DeserializationError err = deserializeJson(doc, json_input);
    if (err) {
      Serial.print("Error: ");
      Serial.println(err.c_str());
      return;
    }
    gps.latitude = doc["lat"];
    gps.longitude = doc["lon"];
  }
}

void convDataToJson(sensor_data &sensor, uint8_t from, uint trans_packets, uint recv_packets, float trans_rate) {
}

void setup() {
  Serial.begin(9600);
  if (!manager.init())
    Serial.println("init failed");
  manager.setRetries(5);
  manager.setTimeout(200);
  manager.addRouteTo(static_node_address, inter_node_address);
  manager.addRouteTo(inter_node_address, inter_node_address);
}

void loop() {
  if (manager.available()) {
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (manager.recvfromAck((uint8_t *)&sensor, &len, &from)) {
      StaticJsonDocument<150> doc;
      recv_packets++;
      doc["recv"] = recv_packets;
      doc["from"] = from;
      doc["hum"] = serialized(String(sensor.h, 1));
      doc["temp"] = serialized(String(sensor.t, 1));
      readGpsData();
      int error_code = manager.sendtoWait((uint8_t *)&gps, send_len, from);
      doc["code"] = error_code;
      if (error_code == RH_ROUTER_ERROR_NONE) {
        trans_packets++;
      }
      retrans_packets = manager.retransmissions();
      total_trans_packets = retrans_packets + trans_packets;
      trans_rate = (float(recv_packets) / float(total_trans_packets)) * 100.0;
      doc["sent"] = total_trans_packets;
      doc["rate"] = serialized(String(trans_rate, 2));
      json_output = "";
      serializeJson(doc, json_output);
      Serial.print(json_output + "\n");
    }
  }
}