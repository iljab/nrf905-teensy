#include <RHRouter.h>
#include <RH_NRF905.h>
#include <SPI.h>
#include "DHT.h"
#include "ArduinoJson.h"

#define RH_ROUTER_MAX_MESSAGE_LEN 22

#define static_node_address 1
#define mobile_node_address 2
#define inter_node_address 3

#define DHTPIN 3          // pin connected to the DHT sensor
#define DHTTYPE DHT22

// Singleton instance of the radio driver
RH_NRF905 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHRouter manager(driver, static_node_address);

DHT dht(DHTPIN, DHTTYPE);

// Dont put this on the stack:
uint8_t buf[RH_ROUTER_MAX_MESSAGE_LEN];

uint trans_packets = 0;
uint total_trans_packets = 0;
uint retrans_packets = 0;
uint recv_packets = 0;
float trans_rate = 0.0;
String json_output;

struct sensor_data{
  float h;
  float t;
};

struct gps_data{
  // long date = 0;
  // long time = 0;
  double latitude = 0;
  double longitude = 0;
};

struct gps_data gps;
struct sensor_data sensor;

uint8_t send_len = sizeof(sensor);

void readSensorData() {
  sensor.h = dht.readHumidity();
  // Read temperature as Celsius (default)
  sensor.t = dht.readTemperature();
  // Check if any reads failed and exit early (to try again).
  if (isnan(sensor.h) || isnan(sensor.t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
}

void setup() 
{
  Serial.begin(9600);
  if (!manager.init())
    Serial.println("init failed");
  // Defaults after init are 433.2 MHz (channel 108), -10dBm
  Serial.println(F("DHT22 test!"));
  dht.begin();
  manager.setRetries(5);
  manager.setTimeout(200);
  manager.addRouteTo(mobile_node_address, inter_node_address);
  manager.addRouteTo(inter_node_address, inter_node_address);
}

void loop()
{
  StaticJsonDocument<200> doc;
  readSensorData();
  doc["hum"] = serialized(String(sensor.h, 1));
  doc["temp"] = serialized(String(sensor.t, 1));
  int error_code;
  error_code = manager.sendtoWait((uint8_t *)&sensor, send_len, mobile_node_address);
  doc["code"] = error_code;
  doc["to"] = mobile_node_address;
  if (error_code == RH_ROUTER_ERROR_NONE){
    trans_packets++;
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (manager.recvfromAckTimeout((uint8_t *)&gps, &len, 1000, &from))
    {
      recv_packets++;
      doc["from"] = from;
      doc["lat"] = gps.latitude;
      doc["lon"] = gps.longitude;
    }
  }
  retrans_packets = manager.retransmissions();
  total_trans_packets = retrans_packets + trans_packets;
  trans_rate = (float(recv_packets) / float(total_trans_packets)) * 100.0;
  doc["rate"] = serialized(String(trans_rate,2));
  doc["sent"] = total_trans_packets;
  doc["recv"] = recv_packets;
  json_output = "";
  serializeJson(doc, json_output);
  Serial.println(json_output+'\n');
  delay(1000);
}