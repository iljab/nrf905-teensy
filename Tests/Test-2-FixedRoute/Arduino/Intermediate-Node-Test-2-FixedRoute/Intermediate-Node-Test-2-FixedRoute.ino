#include <RHRouter.h>
#include <RH_NRF905.h>
#include <SPI.h>

#define RH_ROUTER_MAX_MESSAGE_LEN 22

#define mobile_node_address 2
#define static_node_address 1
#define inter_node_address 3
 
// Singleton instance of the radio
RH_NRF905 driver;
 
// Class to manage message delivery and receipt, using the driver declared above
RHRouter manager(driver, inter_node_address);
 
void setup() 
{
  Serial.begin(9600);
  if (!manager.init())
    Serial.println("init failed");
  
  // Manually define the routes for this network
  manager.addRouteTo(mobile_node_address, mobile_node_address);  
  manager.addRouteTo(static_node_address, static_node_address);
}
 
uint8_t data[] = "Hello World!";
uint8_t send_len = sizeof(data);
// Dont put this on the stack:
uint8_t buf[RH_ROUTER_MAX_MESSAGE_LEN];
 
void loop()
{
  uint8_t recv_len = sizeof(buf);
  uint8_t from;
  if (manager.recvfromAck(buf, &recv_len, &from))
  {
    Serial.print("got request from : 0x");
    Serial.print(from, HEX);
    Serial.print(": ");
     Serial.println((char*)buf);
 
    // Send a reply back to the originator client
    uint8_t errorcode;
    errorcode = manager.sendtoWait(data, send_len, from);
    if (errorcode == RH_ROUTER_ERROR_NONE){
      Serial.println("No errors, message has been sent out to the next hop");
    } else if (errorcode == RH_ROUTER_ERROR_INVALID_LENGTH){
      Serial.print("Invalid length of the message! Max message length: ");
      Serial.print(RH_ROUTER_MAX_MESSAGE_LEN);
      Serial.print(", your message length: ");
      Serial.println(send_len);
    } else if (errorcode == RH_ROUTER_ERROR_NO_ROUTE){
      Serial.print("No route found in the local routing table to node 0x");
      Serial.println(from,HEX);
    } else if (errorcode == RH_ROUTER_ERROR_TIMEOUT) {
      Serial.println("Timeout has expired");
    } else if (errorcode == RH_ROUTER_ERROR_NO_REPLY) {
      Serial.println("No reply received");
    } else if (errorcode == RH_ROUTER_ERROR_UNABLE_TO_DELIVER) {
      Serial.println("Unable to deliver the message to the next hop");
    }
      
  }
}