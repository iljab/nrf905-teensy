import io
import serial
import json
from colorama import Fore as FORE, Back as BACK, Style as STYLE
import logging
import time
import multiprocessing
from signal import signal, SIGINT
from sys import exit as sys_exit
import argparse
import os

script_name = os.path.basename(__file__)[:-3]
script_dir = os.path.dirname(__file__)
time_now = str(time.localtime()[3]) + '-' + str(time.localtime()[4]) + '-' + str(time.localtime()[5]) + '-' + str(time.localtime()[2]) + '-' + str(time.localtime()[1]) + '-' + str(time.localtime()[0])

# definition of the command line variables
try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", required=False, action='store_true', default=False, help="set the level of verbosity, if true debug messages will be shown [default: not set]")
    parser.add_argument("-p", "--port", required=False, default="/dev/ttyACM0",help="sets the path of the port of the connected Teensy MCU [default: /dev/ttyACM0]")
    parser.add_argument("-b", "--baudrate", required=False, default=9600, choices=[9600, 115200],
                            type=int, help="sets the baud rate of the connected Teensy MCU [default: 9600]")
    parser.add_argument("-n", "--name", required=False, type=str, default=script_name+'-'+time_now+".log", help="sets the name of the log file [default: script-name+time.log]")
    parser.add_argument("-d", "--dir", required=False, default="logs/", type=str, help="sets the path to the directory where you want to save the log file to [default: logs/]" )
    parser.add_argument("-l", "--log", required=False, default=False, action='store_true', help="sets the whether or not output will be written to a log file [default: not set] ")
    args = parser.parse_args()

    verbose = args.verbose
    log_name = args.name
    log_path = args.dir
    write_log = args.log
    mcu_port = args.port
    mcu_baudrate = args.baudrate
except Exception as e:
    print(FORE.RED + "+++ MISSING OR WRONG PARAMETERS, SEE %s -h FOR HELP "
                                "FOR MORE INFORMATION. +++\nERROR: %s", script_name, str(e))

if verbose == True:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.DEBUG,
        handlers = [logging.FileHandler(filename=log_path+log_name, mode='w'), logging.StreamHandler()])
    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.DEBUG)
else:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.INFO,
        handlers = [logging.FileHandler(filename=log_path+log_name, mode='w'), logging.StreamHandler()])

    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.INFO)


def readGpsSendTeensy():
    while True:
        try:
            # gps_data = sio_gps.readline()
            gps_data = "$GPRMC,162614,A,5230.5900,N,01322.3900,E,10.0,90.0,131006,1.2,E,A*13"
            # logging.info(gps_data)
            if gps_data[0:6] == "$GPRMC":
                gps_fields = gps_data.split(',')
                # print(gps_fields)
                if gps_fields[2] == 'A':
                    logging.debug("Got GPS Fix!")
                    lat = gps_fields[3]
                    lon = gps_fields[5]
                    lat_ind = gps_fields[4]
                    lon_ind = gps_fields[6]

                    # degree
                    lat_d = int(float(lat)/100)
                    lon_d = int(float(lon)/100)
                    # seconds
                    lat_s = float(lat) - lat_d * 100
                    lon_s = float(lon) - lon_d * 100
                    # decimal degrees
                    lat_dec = lat_d + lat_s/60
                    lon_dec = lon_d + lon_s/60

                    if (lat_ind == 'S'):
                        lat_dec = -lat_dec

                    if (lon_ind == 'W'):
                        lon_dec = -lon_dec

                    gps_json = json.dumps({"lat": lat_dec, "lon": lon_dec})
                    logging.debug("Sending GPS data to Teensy: " + gps_json)
                    try:
                        sio_mcu.write((gps_json+'\n').encode())
                        sio_mcu.flush()
                        logging.debug("GPS data has been sent to Teensy!")
                    except Exception as e:
                        logging.error(FORE.RED + 'ERROR - Sending JSON data failed: {}'.format(e) + FORE.RESET)
                        break
                else:
                    logging.warning(FORE.YELLOW + "No GPS Fix yet!" + FORE.RESET)

            else:
                pass

        except KeyboardInterrupt:
            logging.error(FORE.RED + "Program terminated by pressing CTRL-C" + FORE.RESET)
            pass

        time.sleep(0.001)
    

def readMessage():
    while True:
        try:
            logging.debug("Checking for message...")
            mcu_msg = ""
            while not "code" in mcu_msg:
                mcu_msg = sio_mcu.readline()
                time.sleep(0.001)

            logging.debug("Message received")
            logging.debug(mcu_msg)
            try:
                msg_json = json.loads(mcu_msg)
                temp = msg_json["temp"]
                hum = msg_json["hum"]
                recv = msg_json["recv"]
                from_addr = msg_json["from"]
                code = msg_json["code"]
                rate = msg_json["rate"]
                sent = msg_json["sent"]
                if code == 5:
                    logging.debug('Unable to deliver the message to the next hop!')
                elif code == 4:
                    logging.debug('No reply received!')
                elif code == 3:
                    logging.debug('Timeout has expired!')
                elif code == 2:
                    logging.debug('No route found in the local routing table to the destination node!')
                elif code == 1:
                    logging.debug('Invalid length of the message!')
                elif code == 0:
                    logging.debug('Message has been sent to the next hop!')
                
                msg_recv = "Recv " + str(recv) + " From: 0x" + str(from_addr) + " Hum: " + str(hum) + " % Temp: " + str(temp) + " C "
                logging.info(FORE.YELLOW + msg_recv + FORE.RESET)
                msg_sent = "Sent: " + str(sent) + " To: 0x" + str(from_addr) + " code: " + str(code) + " transmission rate: " + str(rate) + " %\n"
                logging.info(FORE.GREEN + msg_sent + FORE.RESET)
            except Exception as e:
                logging.error(FORE.RED + 'ERROR - Reading JSON failed: {}'.format(e) + FORE.RESET)
            
        except KeyboardInterrupt:
            logging.error(FORE.RED + "Program terminated by pressing CTRL-C" + FORE.RESET)
            pass


def terminate_proc(signal_received, frame):
    """
    Explicitly terminate child processes prior to exiting program.
    """
    print("SIGINT or CTRL-C detected. Exiting gracefully")
    print("Terminating child process 1 - PID: ", child_process_1.pid)
    child_process_1.terminate()
    child_process_1.join()
    print("Terminating child process 2 - PID: ", child_process_2.pid)
    child_process_2.terminate()
    child_process_2.join()
    sys_exit(0)


if __name__ == '__main__':
    try:
        ser_mcu = serial.Serial(mcu_port, mcu_baudrate, timeout=0)
        sio_mcu = io.TextIOWrapper(io.BufferedRWPair(ser_mcu, ser_mcu))
        ser_gps = serial.Serial("/dev/serial0", 9600, timeout=1)
        sio_gps = io.TextIOWrapper(io.BufferedRWPair(ser_gps, ser_gps))
        signal(SIGINT, terminate_proc)
        child_process_1 = multiprocessing.Process(name="child_process_1", target=readGpsSendTeensy)
        child_process_2 = multiprocessing.Process(name="child_process_2", target=readMessage)
        child_process_1.start()
        child_process_2.start()
        print("Press ctrl-c to terminate program.")
    except serial.SerialException as e:
        logging.error(FORE.RED + 'ERROR - Reading from serial port failed: {}'.format(e) + FORE.RESET)
