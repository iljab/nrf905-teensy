import io
import os
import serial
import json
from colorama import Fore as FORE, Back as BACK, Style as STYLE
import time
from math import radians, cos, sin, asin, sqrt
import logging
import argparse

script_name = os.path.basename(__file__)[:-3]
script_dir = os.path.dirname(__file__)
time_now = str(time.localtime()[3]) + '-' + str(time.localtime()[4]) + '-' + str(time.localtime()[5]) + '-' + str(time.localtime()[2]) + '-' + str(time.localtime()[1]) + '-' + str(time.localtime()[0])

# definition of the command line variables
try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", required=False, action='store_true', default=False, help="set the level of verbosity, if true debug messages will be shown [default: not set]")
    parser.add_argument("-p", "--port", required=False, default="/dev/ttyACM0",help="sets the path of the port of the connected Teensy MCU [default: /dev/ttyACM0]")
    parser.add_argument("-b", "--baudrate", required=False, default=9600, choices=[9600, 115200], type=int, help="sets the baud rate of the connected Teensy MCU [default: 9600]")
    parser.add_argument("-n", "--name", required=False, type=str, default=script_name+'-'+time_now+".log", help="sets the name of the log file [default: script-name+time.log]")
    parser.add_argument("-d", "--dir", required=False, default="logs/", type=str, help="sets the path to the directory where you want to save the log file to [default: logs/]" )
    parser.add_argument("-l", "--log", required=False, default=False, action='store_true', help="sets the whether or not output will be written to a log file [default: not set] ")
    parser.add_argument("-x", "--latitude", required=False, type=float, default=0.0, help="sets the fixed latitude [default: 0.0]")
    parser.add_argument("-y", "--longitude", required=False, type=float, default=0.0, help="sets the fixed longitude [default: 0.0]")
   
    args = parser.parse_args()

    verbose = args.verbose
    log_name = args.name
    log_path = args.dir
    write_log = args.log
    mcu_port = args.port
    mcu_baudrate = args.baudrate
    lat_stat = args.latitude
    lon_stat = args.longitude
except Exception as e:
    print(FORE.RED + "+++ MISSING OR WRONG PARAMETERS, SEE %s -h FOR HELP "
                                "FOR MORE INFORMATION. +++\nERROR: %s", script_name, str(e))

if verbose == True:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.DEBUG,
        handlers = [logging.FileHandler(filename=log_path+log_name, mode='w'), logging.StreamHandler()])
    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.DEBUG)
else:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.INFO,
        handlers = [logging.FileHandler(filename=log_path+log_name, mode='w'), logging.StreamHandler()])

    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.INFO)


try:
    ser_mcu = serial.Serial(mcu_port, mcu_baudrate, timeout=0)
    sio_mcu = io.TextIOWrapper(io.BufferedRWPair(ser_mcu, ser_mcu))

    while True:
        try:
            mcu_msg = ""
            # logging.debug(mcu_msg)
            while not "code" in mcu_msg:
                mcu_msg = sio_mcu.readline()
                time.sleep(0.001)

                
            try:
                logging.debug('Message received!')
                msg_json = json.loads(mcu_msg)
                sent = msg_json["sent"]
                to_addr = msg_json["to"]
                temp = msg_json["temp"]
                hum = msg_json["hum"]
                rate = msg_json["rate"]
                recv = msg_json["recv"]
                code = msg_json["code"]
                if code == 5:
                    logging.debug('Unable to deliver the message to the next hop!')
                elif code == 4:
                    logging.debug('No reply received!')
                elif code == 3:
                    logging.debug('Timeout has expired!')
                elif code == 2:
                    logging.debug('No route found in the local routing table to the destination node!')
                elif code == 1:
                    logging.debug('Invalid length of the message!')
                elif code == 0:
                    logging.debug('Message has been sent to the next hop!')
                    try:
                        lat_mob = msg_json["lat"]
                        lon_mob = msg_json["lon"]
                        from_addr = msg_json["from"]
                        if lat_mob > 0.0 and lon_mob > 0.0:
                            lat_stat_rad = radians(lat_stat)
                            lon_stat_rad = radians(lon_stat)
                            lat_mob_rad = radians(lat_mob)
                            lon_mob_rad = radians(lon_mob)
                            # Haversine formula
                            dlon = lon_mob_rad - lon_stat_rad
                            dlat = lat_mob_rad - lat_stat_rad
                            a = sin(dlat / 2)**2 + cos(lat_stat_rad) * cos(lat_mob_rad) * sin(dlon / 2)**2
                            c = 2 * asin(sqrt(a))
                            # Radius of earth in kilometers. Use 3956 for miles
                            r = 6371
                            # calculate the result
                            distance = "{:.4f}".format(c * r)
                            msg_recv = "Recv: " + str(recv) + " From: 0x" + str(from_addr) + " Lat: " + str(lat_mob) + " Lon: " + str(lon_mob) + " Distance: " + str(distance) + " km "
                        else:
                            logging.debug('Lat and Lon are not greater than 0, cannot calculate the distance!')
                            msg_recv = "Recv: " + str(recv) + " From: 0x" + str(from_addr) + " Lat: " + str(lat_mob) + " Lon: " + str(lon_mob) 
                        logging.info(FORE.YELLOW + msg_recv + FORE.RESET)
                    except Exception as e:
                        logging.error(FORE.RED + 'Not received lat and lon!' + FORE.RESET)

                msg_sent = "Sent: " + str(sent) + " To: 0x" + str(to_addr) + " Humidity: " + str(hum) + " % Temp: " + str(temp) + " C" + " code: " + str(code) + " Recv: " + str(recv) +" transmission rate: " + str(rate) + " %"
                logging.info(FORE.GREEN + msg_sent + FORE.RESET)
                
            except Exception as e:
                logging.error(FORE.RED + 'ERROR - Reading JSON failed: {}'.format(e) + FORE.RESET)

        except serial.SerialException as e:
            logging.error(FORE.RED + 'ERROR - Reading from serial port failed: {}'.format(e) + FORE.RESET)
            break

        except KeyboardInterrupt:
            logging.error("\nProgram terminated by pressing ctrl-c")
            break

        time.sleep(0.001)

except Exception as e:
    logging.error(FORE.RED + "Failed to open Serial port: " + mcu_port + FORE.RESET)

