// Stationary-Node-Test-1-P2P.ino 
// Using the RadioHead library by Mike McCauley 

#include <RHReliableDatagram.h>
#include <RH_NRF905.h>
#include <SPI.h>
#include "DHT.h"
#include "ArduinoJson.h"

#define static_node_address 1
#define mobile_node_address 2
#define DHTPIN 3          // pin connected to the DHT sensor
#define DHTTYPE DHT22

// Singleton instance of the radio driver
RH_NRF905 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, static_node_address);

DHT dht(DHTPIN, DHTTYPE);

void setup() 
{
  Serial.begin(9600);
  if (!manager.init())
    Serial.println("init failed");
  // Defaults after init are 433.2 MHz (channel 108), -10dBm
  Serial.println(F("DHT22 test!"));
  dht.begin();
  // manager.setRetries(5);
  // manager.setTimeout(100);
}

// Dont put this on the stack:
uint8_t buf[RH_NRF905_MAX_MESSAGE_LEN];

struct sensor_data{
  float h;
  float t;
};

struct gps_data{
  double latitude = 0;
  double longitude = 0;
};

struct gps_data gps;

struct sensor_data sensor;

uint trans_packets = 0;
uint total_trans_packets = 0;
uint retrans_packets = 0;
uint recv_packets = 0;
float trans_rate = 0.0;
String json_output;

void loop()
{
  // Wait a few seconds between measurements.
  // delay(500);

  sensor.h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  sensor.t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(sensor.h) || isnan(sensor.t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  StaticJsonDocument<200> doc;

  doc["hum"] = serialized(String(sensor.h, 1));
  doc["temp"] = serialized(String(sensor.t, 1));
  
  uint8_t error_code;
  error_code = manager.sendtoWait((uint8_t *)&sensor, sizeof(sensor), mobile_node_address);
  manager.waitPacketSent();
  doc["code"] = error_code;
  doc["to"] = mobile_node_address;
  if (error_code == true){
    trans_packets++;
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (manager.recvfromAckTimeout((uint8_t *)&gps, &len, 500, &from))
    {
      recv_packets++;
      doc["from"] = from;
      doc["lat"] = gps.latitude;
      doc["lon"] = gps.longitude;
    }
  }
  retrans_packets = manager.retransmissions();
  total_trans_packets = retrans_packets + trans_packets;
  trans_rate = (float(trans_packets)/float(recv_packets)) * 100.0;
  doc["rate"] = serialized(String(trans_rate,2));
  doc["success_sent"] = trans_packets;
  doc["total_sent"] = total_trans_packets;
  doc["recv"] = recv_packets;
  json_output = "";
  serializeJson(doc, json_output);
  Serial.print(json_output+'\n');
  delay(1000);
}