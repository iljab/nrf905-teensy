// Mobile-Node-Test-1-P2P.ino 
// Using the RadioHead library by Mike McCauley 

#include <RHReliableDatagram.h>
#include <RH_NRF905.h>
#include <SPI.h>
#include <ArduinoJson.h>

#define mobile_node_address 2

// Singleton instance of the radio driver
RH_NRF905 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, mobile_node_address);

struct sensor_data{
  float h;
  float t;
};

struct sensor_data sensor;

struct gps_data{
  double latitude;
  double longitude;
};

struct gps_data gps;

uint8_t send_len = sizeof(gps);

// Dont put this on the stack:
uint8_t buf[RH_NRF905_MAX_MESSAGE_LEN];

uint recv_packets = 0;
uint trans_packets = 0;
String json_output;
String json_input;

void readGpsData()
{
  // json_input = "{\"lat\": 52.5098, \"lon\": 13.3731}";
  // while (!Serial.available()){}
  if (Serial.available()) {
    json_input = "";
    json_input = Serial.readStringUntil('\n');
    // Serial.print(json_input+"\n");
    StaticJsonDocument<96> doc;
    deserializeJson(doc, json_input);
    DeserializationError err = deserializeJson(doc, json_input);
    if (err) {
      Serial.print("Error: ");
      Serial.println(err.c_str());
      return;
    }
    gps.latitude = doc["lat"];
    gps.longitude = doc["lon"];
  }
}

void setup() 
{
  Serial.begin(9600);
  if (!manager.init())
    Serial.println("init failed");
  // manager.setRetries(5);
  // manager.setTimeout(100);
}

void loop()
{
  readGpsData();
  if (manager.available()){
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (manager.recvfromAck((uint8_t *)&sensor, &len, &from))
    {
      StaticJsonDocument<320> doc;
      recv_packets++;
      doc["recv"] = recv_packets;
      doc["from"] = from;
      doc["hum"] = serialized(String(sensor.h, 1));
      doc["temp"] = serialized(String(sensor.t, 1));
      int error_code = manager.sendtoWait((uint8_t *)&gps, send_len, from); 
      doc["code"] = error_code;
      if (error_code == true){
        trans_packets++;
      }
      uint retrans_packets = manager.retransmissions();
      uint total_trans_packets = retrans_packets + trans_packets;
      float trans_rate = float(trans_packets)/float(recv_packets) * 100.0;
      doc["lat"] = gps.latitude;
      doc["lon"] = gps.longitude;
      doc["success_sent"] = trans_packets;
      doc["total_sent"] = total_trans_packets;
      doc["rate"] = serialized(String(trans_rate,2));
      json_output = "";
      serializeJson(doc, json_output);    
      Serial.print(json_output+"\n");
    }
  }
}
