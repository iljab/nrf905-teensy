import io
import serial
import json
from colorama import Fore as FORE, Back as BACK, Style as STYLE
import logging
import time
import multiprocessing
import threading
from signal import signal, SIGINT
from sys import exit as sys_exit
import argparse
import os

script_name = os.path.basename(__file__)[:-3]
script_dir = os.path.dirname(__file__)
time_now = str(time.localtime()[3]) + '-' + str(time.localtime()[4]) + '-' + str(time.localtime()[5]) + '-' + str(time.localtime()[2]) + '-' + str(time.localtime()[1]) + '-' + str(time.localtime()[0])

# definition of the command line variables
try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", required=False, action='store_true', default=False, help="set the level of verbosity, if true debug messages will be shown [default: not set]")
    parser.add_argument("-p", "--port", required=False, default="/dev/ttyACM0",help="sets the path of the port of the connected Teensy MCU [default: /dev/ttyACM0]")
    parser.add_argument("-b", "--baudrate", required=False, default=9600, choices=[9600, 115200],
                            type=int, help="sets the baud rate of the connected Teensy MCU [default: 9600]")
    parser.add_argument("-n", "--name_log", required=False, type=str, default=script_name + '-' + time_now + ".log", help="sets the name of the log file [default: script-name+time.log]")
    parser.add_argument("-d", "--dir_log", required=False, default="logs/", type=str, help="sets the path to the directory where you want to save the log file to [default: logs/]" )
    parser.add_argument("-N", "--name_output", required=False, type=str, default=script_name + '-' + time_now + ".out", help="sets the name of the output file [default: script-name+time.out]")
    parser.add_argument("-D", "--dir_output", required=False, default="output/", type=str, help="sets the path to the directory where you want to save the output file to [default: output/]" )
    parser.add_argument("-l", "--log", required=False, default=False, action='store_true', help="sets the whether or not output will be written to a log file [default: not set] ")    
    parser.add_argument("-o", "--output", required=False, default=False, action='store_true', help="sets the whether or not json output will be written to a output file [default: not set] ")
    args = parser.parse_args()

    verbose = args.verbose
    name_log = args.name_log
    name_output = args.name_output
    dir_output = args.dir_output
    dir_log = args.dir_log
    write_log = args.log
    write_output = args.output
    mcu_port = args.port
    mcu_baudrate = args.baudrate
except Exception as e:
    print("MISSING OR WRONG PARAMETERS: %s, SEE %s -h FOR HELP AND MORE INFORMATION.", str(e), script_name)

if verbose == True:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.DEBUG,
        handlers = [logging.FileHandler(filename=dir_log+name_log, mode='w'), logging.StreamHandler()])
    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.DEBUG)
else:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.INFO,
        handlers = [logging.FileHandler(filename=dir_log+name_log, mode='w'), logging.StreamHandler()])

    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.INFO)


def readGpsSendTeensy():
    while True:
        try:
            # gps_data = ser_gps.readline().decode("utf-8")
            gps_data = "$GPRMC,162614,A,5230.5900,N,01322.3900,E,10.0,90.0,131006,1.2,E,A*13\n"
            # logging.info(gps_data)
            if gps_data[0:6] == "$GPRMC":
                gps_fields = gps_data.split(',')
                if gps_fields[2] == 'A':
                    logging.debug("Got GPS Fix!")
                    lat = gps_fields[3]
                    lon = gps_fields[5]
                    lat_ind = gps_fields[4]
                    lon_ind = gps_fields[6]
                    # degree
                    lat_d = int(float(lat)/100)
                    lon_d = int(float(lon)/100)
                    # seconds
                    lat_s = float(lat) - lat_d * 100
                    lon_s = float(lon) - lon_d * 100
                    # decimal degrees
                    lat_dec = lat_d + lat_s/60
                    lon_dec = lon_d + lon_s/60
                    if (lat_ind == 'S'):
                        lat_dec = -lat_dec
                    if (lon_ind == 'W'):
                        lon_dec = -lon_dec
                    gps_json = json.dumps({"lat": lat_dec, "lon": lon_dec})
                    logging.debug("Sending GPS data to Teensy: " + gps_json)
                    try:
                        lock.acquire(timeout=0.1)
                        ser_mcu.write((gps_json+"\n").encode())
                        logging.debug("GPS data has been sent to Teensy!")
                        lock.release()
                    except Exception as e:
                        logging.error('Sending JSON data failed: {}'.format(e))
                else:
                    logging.warning("No GPS Fix yet!")
            else:
                pass
        except KeyboardInterrupt:
            logging.error("Program terminated by pressing CTRL-C")
            break
        time.sleep(1)

def readMessage():
    while True:
        try:
            logging.debug("Checking for message...")
            mcu_msg = ""
            while not "code" in mcu_msg:
                lock.acquire()
                mcu_msg = ser_mcu.readline().decode("utf-8")
                time.sleep(0.001)
            lock.release()
            logging.debug("Message received")
            logging.debug(mcu_msg)
            try:
                msg_json = json.loads(mcu_msg)
                lat_mob = msg_json["lat"]
                lon_mob = msg_json["lon"]
                temp = msg_json["temp"]
                hum = msg_json["hum"]
                recv = msg_json["recv"]
                from_addr = msg_json["from"]
                code = msg_json["code"]
                rate = msg_json["rate"]
                success_sent = msg_json["success_sent"]
                total_sent = msg_json["total_sent"]
                msg_recv = "+++ RECV: " + str(recv) + " FROM: 0x" + str(from_addr) + " HUM: " + str(hum) + " % TEMP: " + str(temp) + "° C +++"
                logging.info(msg_recv )
                msg_sent = "+++ SENT: " + str(success_sent) + " TOTAL SENT: " + str(total_sent) + " TO: 0x" + str(from_addr) + " LAT: " + str(lat_mob) + " LON: " + str(lon_mob) + " CODE: " + str(code) + " PACKET SUCCESS RATE: " + str(rate) + " % +++"
                logging.info(msg_sent)
                if code == 0:
                    logging.warning('Unable to deliver the message to stationary node!')
                elif code == 1:
                    logging.debug('Message has been sent to the stationary node!')
                if write_output == True:
                    logging.debug("Writing received data to file: " + dir_output + name_output)
                    try:
                        with open(dir_output+name_output, 'a') as f:
                            f.write(mcu_msg)
                            logging.debug("Received data has been written to: " +dir_output+name_output)
                    except Exception as e:
                        logging.error('Writing received data to file: '+ dir_output + name_output + ' failed: {}'.format(e))
            except Exception as e:
                logging.error('Reading JSON failed: {}'.format(e))
        except KeyboardInterrupt:
            logging.error("Program terminated by pressing CTRL-C")
            break
        time.sleep(0.001)
        
if __name__ == '__main__':
    try:
        ser_mcu = serial.Serial(mcu_port, mcu_baudrate, timeout=0)
        ser_gps = serial.Serial("/dev/serial0", 9600, timeout=1)
        try:
            lock = threading.Lock()
            thread_1 = threading.Thread(target=readGpsSendTeensy)
            thread_2 = threading.Thread(target=readMessage)
            thread_1.start()
            thread_2.start()
            thread_1.join()
            thread_2.join()
        except Exception as e:
            logging.error('Starting Threads failed: {}'.format(e))
    except serial.SerialException as e:
        logging.error('Reading from serial port failed: {}'.format(e))
