import os
import serial
import json
from colorama import Fore as FORE, Back as BACK, Style as STYLE
import time
from math import radians, cos, sin, asin, sqrt
import logging
import argparse

script_name = os.path.basename(__file__)[:-3]
script_dir = os.path.dirname(__file__)
time_now = str(time.localtime()[3]) + '-' + str(time.localtime()[4]) + '-' + str(time.localtime()[5]) + '-' + str(time.localtime()[2]) + '-' + str(time.localtime()[1]) + '-' + str(time.localtime()[0])

# definition of the command line variables
try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", required=False, action='store_true', default=False, help="set the level of verbosity, if true debug messages will be shown [default: not set]")
    parser.add_argument("-p", "--port", required=False, default="/dev/ttyACM0",help="sets the path of the port of the connected Teensy MCU [default: /dev/ttyACM0]")
    parser.add_argument("-b", "--baudrate", required=False, default=9600, choices=[9600, 115200], type=int, help="sets the baud rate of the connected Teensy MCU [default: 9600]")
    parser.add_argument("-n", "--name_log", required=False, type=str, default=script_name + '-' + time_now + ".log", help="sets the name of the log file [default: script-name+time.log]")
    parser.add_argument("-d", "--dir_log", required=False, default="logs/", type=str, help="sets the path to the directory where you want to save the log file to [default: logs/]" )
    parser.add_argument("-l", "--log", required=False, default=False, action='store_true', help="sets the whether or not output will be written to a log file [default: not set] ")
    parser.add_argument("-x", "--latitude", required=False, type=float, default=0.0, help="sets the fixed latitude [default: 0.0]")
    parser.add_argument("-y", "--longitude", required=False, type=float, default=0.0, help="sets the fixed longitude [default: 0.0]")
    parser.add_argument("-o", "--output", required=False, default=False, action='store_true', help="sets the whether or not json output will be written to a output file [default: not set] ")
    parser.add_argument("-N", "--name_output", required=False, type=str, default=script_name + '-' + time_now + ".out", help="sets the name of the output file [default: script-name+time.out]")
    parser.add_argument("-D", "--dir_output", required=False, default="output/", type=str, help="sets the path to the directory where you want to save the output file to [default: output/]" )

    args = parser.parse_args()

    verbose = args.verbose
    name_log = args.name_log
    dir_log = args.dir_log
    name_output = args.name_output
    dir_output = args.dir_output
    write_output = args.output
    write_log = args.log
    mcu_port = args.port
    mcu_baudrate = args.baudrate
    lat_stat = args.latitude
    lon_stat = args.longitude
except Exception as e:
    print("MISSING OR WRONG PARAMETERS: %s, SEE %s -h FOR HELP AND MORE INFORMATION.", str(e), script_name)


if verbose == True:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.DEBUG,
        handlers = [logging.FileHandler(filename=dir_log+name_log, mode='w'), logging.StreamHandler()])
    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.DEBUG)
else:
    if write_log == True:
        logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level = logging.INFO,
        handlers = [logging.FileHandler(filename=dir_log+name_log, mode='w'), logging.StreamHandler()])

    else:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level = logging.INFO)


try:
    ser_mcu = serial.Serial(mcu_port, mcu_baudrate, timeout=0)

    while True:
        try:
            mcu_msg = ""
            # logging.debug(mcu_msg)
            while not "code" in mcu_msg:
                mcu_msg = ser_mcu.readline().decode("utf-8")
                logging.debug(mcu_msg)
                time.sleep(1)

            try:
                logging.debug('Message received!')
                msg_json = json.loads(mcu_msg)
                total_sent = msg_json["total_sent"]
                success_sent = msg_json["success_sent"]
                to_addr = msg_json["to"]
                temp = msg_json["temp"]
                hum = msg_json["hum"]
                rate = msg_json["rate"]
                recv = msg_json["recv"]
                code = msg_json["code"]
                if code == 0:
                    logging.debug('Unable to deliver the message to mobile node!')
                elif code == 1:
                    logging.debug('Message has been sent to mobile node!')
                    try:
                        lat_mob = msg_json["lat"]
                        lon_mob = msg_json["lon"]
                        from_addr = msg_json["from"]
                        if lat_mob > 0.0 and lon_mob > 0.0:
                            lat_stat_rad = radians(lat_stat)
                            lon_stat_rad = radians(lon_stat)
                            lat_mob_rad = radians(lat_mob)
                            lon_mob_rad = radians(lon_mob)
                            # Haversine formula
                            dlon = lon_mob_rad - lon_stat_rad
                            dlat = lat_mob_rad - lat_stat_rad
                            a = sin(dlat / 2)**2 + cos(lat_stat_rad) * cos(lat_mob_rad) * sin(dlon / 2)**2
                            c = 2 * asin(sqrt(a))
                            # Radius of earth in kilometers. Use 3956 for miles
                            r = 6371
                            # calculate the result
                            distance = (c * r)*1000.0
                            msg_recv = "+++ RECV: " + str(recv) + " FROM: 0x" + str(from_addr) + " LAT: " + str(lat_mob) + " LON: " + str(lon_mob) + " DIST: " + str(distance) + " m +++"
                        else:
                            logging.debug('Lat and Lon are not greater than 0, cannot calculate the distance!')
                            msg_recv = "+++ RECV: " + str(recv) + " FROM: 0x" + str(from_addr) + " LAT: " + str(lat_mob) + " LON: " + str(lon_mob) + " +++"

                        logging.info(msg_recv)
                    except Exception as e:
                        logging.error('Not received lat and lon!, error: {}'.format(e))

                msg_sent = "+++ SUCCESS SENT: " + str(success_sent) + " TOTAL SENT: " + str(total_sent) + " TO: 0x" + str(to_addr) + " HUM: " + str(hum) + " % TEMP: " + str(temp) + "° C" + " CODE: " + str(code) +" PACKET SUCCESS RATE " + str(rate) + " % +++"
                logging.info(msg_sent)

                if write_output == True:
                    file_data_json = json.dumps(
                        {
                            "total-sent": total_sent, 
                            "success-sent": success_sent,
                            "received": recv,
                            "packet-rate": rate,
                            "latitude-mobile": lat_mob,
                            "longitude-mobile": lon_mob,
                            "latitude-static": lat_stat,
                            "longitude-static": lon_stat,
                            "distance(m)": distance,
                            })
                    with open(dir_output+name_output, 'a') as f:
                        f.write(str(file_data_json)+'\n')
                
            except Exception as e:
                logging.error('ERROR - Reading JSON failed: {}'.format(e))

        except serial.SerialException as e:
            logging.error('ERROR - Reading from serial port failed: {}'.format(e))
            break

        except KeyboardInterrupt:
            logging.error("\nProgram terminated by pressing ctrl-c")
            break

        time.sleep(0.001)

except Exception as e:
    logging.error("Failed to open Serial port " + mcu_port + " Error: {}".format(e))

